/**
 * 
 */
package edu.ucsd.cs110s.temerature;

/**
 * @author kaw023
 *
 */
public class Fahrenheit extends Temperature 
{ 
 public Fahrenheit(float t) 
 { 
  super(t); 
 } 
 public String toString() 
 { 
  return Float.toString(getValue()); // return the value of Value
 }
@Override
public Temperature toCelsius() {
	return new Celsius((getValue() - 32 ) * 5 / 9); // return a Celsius 
}
@Override
public Temperature toFahrenheit() {
	return this; // return the Fahrenheit itself
} 
} 
